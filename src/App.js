import React, {Component} from 'react';
import './App.css';

const pages = ['Home', 'Services', 'About', 'Contact US'];

class App extends Component{
  constructor(props){
    super(props);
    this.changePage = this.changePage.bind(this);
  }

  state = {
    page: pages[0]
  }

  changePage(index){
    this.setState({page:pages[index]});
  }

  render(){
    return(
      <div>
        <ul className="header">
            {pages.map((i, index)=>{
              return (
                <li onClick={() =>{
                  this.changePage(index);
            }}>{i}</li>
              )
            })}
        </ul>
          <center><h1>Anda Memilih : {this.state.page}</h1></center>
      </div>
    )
  }

  


}
export default App;